FROM openjdk:8
WORKDIR /usr/src/app
COPY simu.jar ./
COPY . .
CMD [ "java", "-jar", "simu.jar", ".", "80", "http://kong:8000" ]
